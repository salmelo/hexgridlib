# Hex Grid Library #

This is a simple set of scripts designed to get unity projects utilizing hex grids up and running, including data classes for working with Hex coordinates, and layout classes for translating between hex-space and world-space.

This library is heavily derivative of the theory and example code found on [Red Blob Games](http://www.redblobgames.com/grids/hexagons/), but written specifically with Unity and C# in mind.

This project is released under the apache license. See below.

### How do I get set up? ###

* Just download and import [HexGridLib.unitypackage](../../downloads/HexGridLib.unitypackage). It contains the main script (HexGridLib), some editor scripts for better inspectors when working with Hex coordinates, and an example script for getting a grid on screen super fast (HexGridRectangle). As well as a couple textures put together with this library in mind. You should be able to use them as a template for putting together your own.

### The Classes ###

#### Hex ####

This is the struct for storing hex coordinates. There are several operators and static functions for various operations, from addition and comparison to distances and finding neighboring coordinates.

#### PointyDirection & FlatDirection ####

These are just enums to make easier to know which direction you're moving in. One for each of the Pointy top and Flat top orientations.

#### Orientation ####

All the differences between Flat top and Pointy top grid orientations are defined here. Just pick one or the other when you create your layout and you're good to go.

#### Layout ####

This class is responsible for all the conversions between hex coordinates and world space. All it needs is an Orientation, a size (distance from hex center to corner), and an origin point (where in the world the 0,0,0 hex is located).

Note that it was built largely with 2D games in mind, so by default it produces results in XY coordinates. If you want your grid to be along the XZ plane you can pass `true` as the fourth constructor parameter. Alternatively, you can use the methods with names ending in "3D" or "2D" to extract results in the XZ or XY plane, regardless of the Layout's default3d setting.

Keep in mind that 3d grids can have an origin with varying `y` positions. The `Layout` should respect this when outputing 3D coordinates, but it could be lost when converting to and from `Vector2`s. You shouldn't need to worry about it when inputing world-space coordinates, since the grid is still 2-dimensional.

#### Grid ####

Generates grid meshes, with UV coordinates appropriate for an atlased texture of the provided size. Note that this is a fairly simple mesh, if you want advanced features like elevation you will have to roll that yourself.

#### OffsetCoord ####

This is a simple struct for storing offset coordinates, which reference a hex by column and row. There are provided methods for converting to and from standard Hex coordinates. These aren't really used anywhere in the library but can be useful for some setups.

#### ShapeGenerators ####

This class contains a collection of methods for generating lists of hexes in simple shapes: parallelograms, rectangles, hexagons, and triangles.

#### BorderDrawer ####

A static class to help with drawing borders around collections of hexes. See the example scene for basic usage.

#### Extensions ####

This is just a static class for containing useful extension methods. Namely the `To3D` and `To2D`methods, that convert Vectors between the XY and XZ planes.

## License ##

```
Copyright 2016 Salvador A. Melo

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
```